
import pathlib
import sys

import numpy as np
import pandas as pd
import xarray as xr

SOURCE_DIR = pathlib.Path(__file__).absolute().parent

sys.path.append(SOURCE_DIR)
from run_emulator import climate_index


L_RCP =  ["rcp26", "rcp60", "rcp85"]

L_GCM = ["GFDL-ESM2M", "HadGEM2-ES", "IPSL-CM5A-LR", "MIROC5"]

PERIOD = (1980, 2100)

OUTPUT_DIR = pathlib.Path("data")


def main():
    ds_res = None
    for index in ["gmt", "esoi"]:
        res = []
        for rcp in L_RCP:
            for gcm in L_GCM:
                df = climate_index(gcm, rcp, index)
                df['rcp'] = rcp
                df['gcm'] = gcm
                if index == "gmt":
                    df = df.drop(columns=['month'])
                res.append(df)
        index_cols = ['rcp', 'gcm', 'year'] + ([] if index == "gmt" else ["month"])
        ds = (
            pd.concat(res)
            .set_index(index_cols)
            .to_xarray()
            .reindex(year=np.arange(PERIOD[0], PERIOD[1] + 1))
        )
        if ds_res is None:
            ds_res = ds
        else:
            ds_res[index] = ds[index]

    path = OUTPUT_DIR / "climate_index.nc"
    print(f"Writing to {path} ...")
    encoding = {v: {'zlib': True} for v in ds.data_vars}
    ds_res.to_netcdf(path, encoding=encoding)


if __name__ == "__main__":
    main()
