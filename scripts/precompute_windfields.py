
import os
import pathlib

import numpy as np

from climada.hazard import Centroids, TCTracks, TropCyclone

SOURCE_DIR = pathlib.Path(__file__).absolute().parent
DATA_DIR = SOURCE_DIR.parent / 'data'
INPUT_DIR = DATA_DIR / "input"
WINDFIELD_DIR = INPUT_DIR / "windfields"


def global_centroids():
    path = lambda h: WINDFIELD_DIR / f"Trial4_GB_dkipsl_20thcal_{h}_0360as.mat"
    centroids = Centroids.from_mat(path("N"))
    centroids.on_land = np.zeros(centroids.size, dtype=bool)
    centroids.on_land[:1287183] = True
    centroids_S = Centroids()
    centroids_S.read_mat(path("S"))
    centroids_S.on_land = np.zeros(centroids_S.size, dtype=bool)
    centroids_S.on_land[:325229] = True
    centroids.append(centroids_S)
    return centroids.select(sel_cen=(np.abs(centroids.lat) <= 80))


def precompute_ibtracs_windfields(period=(1950, 2019), pool=None):
    """Precompute IBTrACS windfields"""
    tracks = TCTracks.from_ibtracs_netcdf(year_range=period, estimate_missing=True)
    tracks.equal_timestep(time_step_h=1)
    fname = f"GLB_0360as_hazard_ibtracs_{period[0]}-{period[1]}.hdf5"
    path = WINDFIELD_DIR / fname
    windfields = TropCyclone.from_tracks(
        tracks, centroids=global_centroids(), ignore_distance_to_coast=True, pool=pool)
    windfields.write_hdf5(path)


def precompute_emanuel_windfields(tracks_base, pool=None):
    """Precompute windfields from Kerry Emanuel's simulated TC tracks"""
    path = INPUT_DIR / "tracks" / f"{tracks_base}.mat"
    tracks = TCTracks.from_simulations_emanuel(path, hemisphere='both')
    tracks.equal_timestep(time_step_h=1)

    fname = f"GLB_0360as_hazard_{tracks_base.replace('GB_dk', '').replace('cal', '')}.hdf5"
    path = WINDFIELD_DIR / fname
    windfields = TropCyclone.from_tracks(
        tracks, centroids=global_centroids(), ignore_distance_to_coast=True, pool=pool)
    windfields.write_hdf5(path)


if __name__ == "__main__":
    import sys
    if len(sys.argv) == 1:
        print("Specify one of 'ibtracs' or 'emanuel'.")
        sys.exit()

    pool = None
    if 'SLURM_JOB_CPUS_PER_NODE' in os.environ:
        from pathos.pools import ProcessPool as Pool
        pool = Pool(nodes=int(os.environ['SLURM_JOB_CPUS_PER_NODE']))

    if sys.argv[1] == "ibtracs":
        if len(sys.argv) == 4:
            period = (int(sys.argv[2]), int(sys.argv[3]))
        else:
            # for testing purposes
            period = (1990, 2000)
        precompute_ibtracs_windfields(period=period, pool=pool)
    else:
        tracks_base = sys.argv[1]
        if tracks_base == "emanuel":
            # for testing purposes
            tracks_base = "Trial1_GB_dkipsl_20thcal"
        precompute_emanuel_windfields(tracks_base, pool=pool)
