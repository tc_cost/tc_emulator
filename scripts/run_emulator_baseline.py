"""
Use the pre-calibrated emulator (from run_emulator.py) to generate draws that reproduce a
hypothetical baseline scenario, i.e. today's climate conditions held fixed for a specified
period of time.
"""

import pathlib
import pickle

import climada_petals.hazard.emulator.random
import numpy as np
import pandas as pd


def main(emulator_file, gmt_range, nrealizations, draw_period, seed):
    # initialize random state
    climada_petals.hazard.emulator.random.random_state = np.random.RandomState(seed)

    with open(emulator_file, "rb") as fp:
        emu = pickle.load(fp)

    mask = (emu.stats_pred['gmt'] >= gmt_range[0]) & (emu.stats_pred['gmt'] <= gmt_range[1])
    df_baseline = emu.stats_pred[mask]

    intensity_mean = df_baseline['intensity_mean'].mean()
    intensity_std = df_baseline['intensity_mean'].std()
    intensity_std = np.clip(np.abs(intensity_std), 0.5, 10)
    freq_mean = df_baseline['eventcount'].mean()

    print(emulator_file.parent.name)
    print(f"{freq_mean} x ({intensity_mean} ± {intensity_std})")

    emu.stats_pred = pd.DataFrame({
        'year': np.arange(draw_period[0], draw_period[1] + 1),
        'gmt': emu.stats_pred['gmt'].mean(),
        'esoi': emu.stats_pred['esoi'].mean(),
        'intensity_mean': intensity_mean,
        'intensity_mean_residuals': intensity_std,
        'eventcount': freq_mean,
        'eventcount_residuals': 0,
    })

    draws = emu.draw_realizations(nrealizations, draw_period)
    fname = (f"draws_baseline_{gmt_range[0]}-{gmt_range[1]}"
             + f"_{draw_period[0]}-{draw_period[1]}_{nrealizations}.csv")
    draws.to_csv(emulator_file.parent / fname, index=None)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Draw according to baseline.')
    parser.add_argument('emulator_file', metavar="PATH", type=pathlib.Path,
                        help='File with pickled emulator object.')
    parser.add_argument('gmt_range', type=float, metavar="TEMPERATURE", nargs=2, default=None,
                        help='Global mean temperature anomaly range (mintemp, maxtemp).')
    parser.add_argument('nrealizations', type=int, metavar="N",
                        help='Number of realizations.')
    parser.add_argument('period', type=int, metavar="YEAR", nargs=2, default=None,
                        help='Time period (minyear, maxyear).')
    parser.add_argument('--seed', type=int, metavar="INT", default=123456789,
                        help='Random seed for making draws.')
    args = parser.parse_args()

    main(args.emulator_file, args.gmt_range, args.nrealizations, args.period, args.seed)