
import argparse
import pathlib
import sys

SOURCE_DIR = pathlib.Path(__file__).absolute().parent

sys.path.append(SOURCE_DIR)
from run_emulator import climate_index

DATA_DIR = SOURCE_DIR.parent / 'data'
INPUT_DIR = DATA_DIR / "input"


def main():
    parser = argparse.ArgumentParser(description='Create GMT scenario time line from CMIP6.')
    parser.add_argument('gcm', metavar="GCM", type=str,
                        help='Name of CMIP6 GCM.')
    parser.add_argument('rcp', metavar="RCP", type=str,
                        help='Name of RCP/SSP scenario.')
    parser.add_argument('--period', type=int, metavar="YEAR", nargs=2, default=(1950, 2100),
                        help='Time period (minyear, maxyear).')
    args = parser.parse_args()

    path = INPUT_DIR / "scenarios" / (
        f"{args.gcm}_{args.rcp}_GMT-path_Scenario_{args.period[0]}-{args.period[1]}.csv"
    )

    cidx = climate_index(args.gcm, args.rcp, "gmt")
    cidx = cidx[(args.period[0] <= cidx['year']) & (cidx['year'] <= args.period[1])]
    cidx = cidx.rename(columns={"gmt": "50%"})
    cidx = cidx[["year", "50%"]]

    print(f"Writing to {path} ...")
    cidx.to_csv(path, index=None)


if __name__ == "__main__":
    main()
