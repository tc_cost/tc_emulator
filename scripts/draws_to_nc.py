"""Generate NetCDF files with windfields for given draws.csv file"""

import datetime as dt
import pathlib
import re

import netCDF4 as nc
import numpy as np
import pandas as pd

from climada.hazard import TropCyclone
from climada_petals.hazard.emulator import const

SOURCE_DIR = pathlib.Path(__file__).absolute().parent
DATA_DIR = SOURCE_DIR.parent / 'data'
INPUT_DIR = DATA_DIR / "input"

# minimum wind speed in knots for `affected` regions
KNOTS_2_MS = 0.514444
MIN_WIND_L = [34, 48, 64, 96]
MIN_WIND = MIN_WIND_L[0]
MIN_WIND_MS = MIN_WIND * KNOTS_2_MS


def tc_hazard_to_netcdf(hazard, path):
    """Write windfields to NetCDF file

    Parameters
    ----------
    hazard : TropCyclone object
        Windfield hazards.
    path : str
        Path to NetCDF file for output.
    """
    ncfile, nc_indls = prepare_nc(hazard.centroids, path)
    grid_shape = (ncfile['lat'].size, ncfile['lon'].size)
    for ievent, wind_event in enumerate(hazard.intensity):
        wind_nc = np.zeros(grid_shape).ravel()
        wind_nc[nc_indls] = (wind_event / KNOTS_2_MS).toarray()
        wind_nc[wind_nc < MIN_WIND] = 0
        ncfile['wind'][ievent, :, :] = wind_nc.reshape(grid_shape)
        ncfile['time'][ievent] = nc.date2num(dt.datetime.fromordinal(hazard.date[ievent]),
                                             units=ncfile['time'].units,
                                             calendar=ncfile['time'].calendar)
        ncfile['realization'][ievent] = int(hazard.event_name[ievent].split("-")[-1])
    ncfile.close()


def prepare_nc(centroids, path):
    """Prepare a NetCDF file for storing TC windfield data

    Parameters
    ----------
    centroids : Centroids object
    path : str
        Path to output file.

    Returns
    -------
    ncfile : nc.Dataset (opened as writable)
        Dimensions for event, longitude and latitude.
        More variables have to be added.
    nc_indls : ndarray of ints
        The centroids of the original data are not necessarily on a grid, but
        the NetCDF format forces the output to be on a (not necessarily regular)
        grid.
        This output is for identification between the original centroids and
        the grid cells in the NetCDF data. If the coordinates of the original
        centroids are stored in two arrays (lat, lon) and the NetCDF dimensions
        are (nc_lat, nc_lon), then:

        >>> yf, xf = [c.flatten() for c in np.meshgrid(nc_lat, nc_lon, indexing='ij')]
        >>> assert np.all(lat == yf[nc_indls])
        >>> assert np.all(lon == xf[nc_indls])
    """
    lat, lon = centroids.lat, centroids.lon
    lat_uniq, lat_inv = np.unique(lat, return_inverse=True)
    if np.any(lon < -100) and np.any(lon > 100):
        # this region crosses the antimeridian
        # we want to sort centroids west to east, e.g. for 135 till -60:
        # 135,136,...,179,180,-180,-179,...,-61,-60
        lon_uniq, lon_inv = np.unique(lon % 360, return_inverse=True)
        lon_uniq[lon_uniq > 180] -= 360.0
    else:
        lon_uniq, lon_inv = np.unique(lon, return_inverse=True)
    grid_shape = (lat_uniq.shape[0], lon_uniq.shape[0])
    nc_indls = np.ravel_multi_index((lat_inv, lon_inv), dims=grid_shape)

    path.unlink(missing_ok=True)

    print(f"Preparing {path}...")
    ncfile = nc.Dataset(path, 'w', format='NETCDF4')
    ncfile.createDimension('lat', size=grid_shape[0])
    ncfile.createDimension('lon', size=grid_shape[1])
    ncfile.createDimension('event', size=None)

    nc_var = ncfile.createVariable('lat', 'float64', ('lat',), zlib=True)
    nc_var.units = 'degrees_north'
    nc_var.standard_name = 'latitude'
    nc_var[:] = lat_uniq

    nc_var = ncfile.createVariable('lon', 'float64', ('lon',), zlib=True)
    nc_var.units = 'degrees_east'
    nc_var.standard_name = 'longitude'
    nc_var[:] = lon_uniq

    nc_var = ncfile.createVariable('event', 'int64', ('event',), zlib=True)
    nc_var.units = 'consecutively numbered'

    nc_var = ncfile.createVariable('realization', 'int64', ('event',), zlib=True)
    nc_var.units = 'consecutively numbered'

    nc_var = ncfile.createVariable('time', 'int64', ('event',), zlib=True)
    nc_var.units = 'days since 1850-01-01'
    nc_var.calendar = 'gregorian'

    nc_var = ncfile.createVariable('wind', 'float64', ('event', 'lat', 'lon'), zlib=True)
    nc_var.units = 'wind speed in knots'
    nc_var.standard_name = 'TC wind speed'

    return ncfile, nc_indls


def haz_select_bounds(haz, bounds):
    """Make subselection of specified hazard (centroids within specified bounds)"""
    lonmin, lonmax, latmin, latmax = bounds
    lat, lon = haz.centroids.lat, haz.centroids.lon
    if lonmin < lonmax:
        haz.centroids.region_id = (latmin <= lat) & (lat <= latmax) \
                                & (lonmin <= lon) & (lon <= lonmax)
    else:
        # basin crossing the -180/180 longitude threshold
        haz.centroids.region_id = (latmin <= lat) & (lat <= latmax) \
                                & ((lonmin <= lon) | (lon <= lonmax))
    return haz.select(reg_id=1)


def main(draws_path):
    """Main function used when called from the command line"""
    draws = pd.read_csv(draws_path)
    netcdf_path = draws_path.parent / draws_path.name.replace("csv", "nc")
    subbasin = re.match(
        r".*_rcp[0-9]{2}_([A-Z]+)_[0-9]+_.*",
        draws_path.parent.name
    ).group(1)

    # load master hazard object containing all relevant Emanuel tracks
    filenames = np.unique(draws['name'].str.replace("-[0-9]+$", "", regex=True))
    hemisphere = re.match(r".*_([NS])_0360as.*", filenames[0]).group(1)
    hazards = []
    for fname in filenames:
        path = INPUT_DIR / "windfields" / fname
        haz = TropCyclone.from_mat(path)
        haz.event_name = [f"{fname}-{n}" for n in haz.event_name]
        # some datasets include centroids beyond 60° that are irrelevant for TC hazards
        cutidx = 901186 if hemisphere == 'N' else 325229
        haz.centroids.region_id = np.zeros_like(haz.centroids.lat)
        haz.centroids.region_id[:cutidx] = 1
        haz = haz.select(reg_id=1)
        haz = haz_select_bounds(haz, const.TC_BASIN_GEOM_SIMPL[subbasin][0])
        haz.basin = [subbasin]*len(haz.event_name)
        hazards.append(haz)
    haz = TropCyclone.concat(hazards)
    haz = haz.select(event_names=list(draws['name'].values))
    haz.event_name = [f"{n}-{i}" for n, i in zip(haz.event_name, draws['real_id'].values)]

    years = [dt.datetime.fromordinal(d).year for d in haz.date]
    date_diff = [dt.datetime(y_dst, 1, 1).toordinal() - dt.datetime(y, 1, 1).toordinal() \
                 for y_dst, y in zip(draws['year'].values, years)]
    haz.date += date_diff

    haz.check()

    tc_hazard_to_netcdf(haz, netcdf_path)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Generate NetCDF windfield file from draws.')
    parser.add_argument('path', metavar="PATH", type=pathlib.Path,
                        help='The draws CSV file to use.')
    args = parser.parse_args()

    main(args.path)
