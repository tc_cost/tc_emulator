"""
Use the pre-calibrated emulator (from run_emulator.py) to generate draws that reproduce a
hypothetical climate scenario, specified by a GMT-time series
"""

import pathlib
import pickle
import sys

import climada_petals.hazard.emulator.random
import numpy as np
import pandas as pd

sys.path.append(pathlib.Path(__file__).absolute().parent)
from run_emulator import climate_index


def main(emulator_file, gmt_file, nrealizations, draw_period, seed, name=None):
    if name is None:
        name = gmt_file.stem

    # initialize random state
    climada_petals.hazard.emulator.random.random_state = np.random.RandomState(seed)

    with open(emulator_file, "rb") as fp:
        emu = pickle.load(fp)

    gmt_df = pd.read_csv(gmt_file).rename(columns={"50%": "gmt"})
    gmt_df['month'] = 7

    draws = []
    for i_esoi, esoi in enumerate([-2.0, -0.5, 2.0]):
        esoi_df = gmt_df.copy().rename(columns={"gmt": "esoi"})
        esoi_df['esoi'] = esoi
        emu.predict_statistics([gmt_df, esoi_df])
        draws_esoi = emu.draw_realizations(nrealizations, draw_period)
        draws_esoi['real_id'] += i_esoi * nrealizations
        draws.append(draws_esoi)
    draws = (
        pd.concat(draws)
        .sort_values(by=["year", "real_id"])
        .reset_index(drop=True)
    )

    path = emulator_file.parent / (
        f"draws_scenario_{name}_{draw_period[0]}-{draw_period[1]}_{nrealizations}.csv"
    )
    print(f"Writing to {path} ...")
    draws.to_csv(path, index=None)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Draw according to baseline.')
    parser.add_argument('emulator_file', metavar="EMULATOR_FILE", type=pathlib.Path,
                        help='File with pickled emulator object.')
    parser.add_argument('gmt_file', metavar="SCENARIO_FILE", type=pathlib.Path,
                        help="CSV-file with scenario's GMT time series.")
    parser.add_argument('nrealizations', type=int, metavar="N",
                        help='Number of realizations.')
    parser.add_argument('period', type=int, metavar="YEAR", nargs=2, default=None,
                        help='Time period (minyear, maxyear).')
    parser.add_argument('--seed', type=int, metavar="INT", default=123456789,
                        help='Random seed for making draws.')
    parser.add_argument('--name', type=str, metavar="NAME", default=None,
                        help='Name of the scenario used in the output file name.')
    args = parser.parse_args()

    main(args.emulator_file, args.gmt_file, args.nrealizations, args.period, args.seed,
         name=args.name)
