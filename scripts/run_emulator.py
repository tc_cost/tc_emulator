"""
For this script, place the precomputed windfields for the ISIMIP TC tracks
in ./data/input/windfields/*.mat and the precomputed windfields for the
ibtracs TCs in ./data/input/windfields/GLB_0360as_hazard_1950-2015.mat.

The climate index time series for the different GCMs and RCPs should be
available in ./data/input/climate_index/*.csv, see "scripts/index_extract.py".
"""

import datetime as dt
import pathlib
import pickle
import re
import sys

import h5py
import numpy as np
import pandas as pd
import scipy.io
import shapely

from climada.hazard import TropCyclone
from climada.hazard.base import DEF_VAR_MAT
from climada_petals.hazard.emulator import const
from climada_petals.hazard.emulator import emulator
from climada_petals.hazard.emulator import geo
from climada_petals.hazard.emulator import stats
import climada_petals.hazard.emulator.random

SOURCE_DIR = pathlib.Path(__file__).absolute().parent

sys.path.append(SOURCE_DIR)
from index_extract import PICONTROL_PERIOD

DATA_DIR = SOURCE_DIR.parent / 'data'
INPUT_DIR = DATA_DIR / "input"
OUTPUT_DIR = DATA_DIR / "output"

# minimum wind speed in knots for `affected` regions
KNOTS_2_MS = 0.514444
MIN_WIND_L = [34, 64, 96]
MIN_WIND = MIN_WIND_L[0]
MIN_WIND_MS = MIN_WIND * KNOTS_2_MS

GCM_TRANS = {
    'HadGEM2-ES': 'had',
    'MIROC5': 'miroc',
    'GFDL-ESM2M': 'gfdl',
    'IPSL-CM5A-LR': 'ipsl',
}

GCM_TRANS_TEMP = {
    'HadGEM2-ES': 'hadgem',
    'MIROC5': 'miroc',
    'GFDL-CM3': 'gfdl5',
    'CCSM4': 'ccsm4',
    'MRI-CGCM3': 'mri',
    'MPI-ESM-MR': 'mpi',
}


def gcm_trans(gcm, temp):
    """Convert full GCM names to abbreviations"""
    trans = GCM_TRANS_TEMP if temp else GCM_TRANS
    return trans[gcm]


def gcm_trans_inv(gcm, temp):
    """Convert GCM abbreviations to full names"""
    trans = GCM_TRANS_TEMP if temp else GCM_TRANS
    trans_inv = {v: k for k, v in trans.items()}
    return trans_inv[gcm]


def ibtracs_id2meta(id_int):
    """Derive storm meta data from ibtracs storm ID (int)"""
    id_str = str(int(id_int))
    hemisphere = 'N' if id_str[7] == '0' else 'S'
    id_str = id_str[:7] + hemisphere + id_str[8:]
    year = int(id_str[:4])
    days = int(id_str[4:7])
    date = dt.datetime(year, 1, 1) + dt.timedelta(days - 1)
    return (id_str, year, date.month, date.day, hemisphere)


def ibtracs_windfields(region, period=None):
    """Load subset of precomputed windfields for ibtracs TCs (1950-2015)

    Parameters
    ----------
    region : TCRegion object
        The geographical region to consider.
    period : pair of ints (minyear, maxyear)
        First and last year to consider.

    Returns
    -------
    windfields : climada.hazard.TropCyclone object
    """
    var_names = DEF_VAR_MAT
    var_names['var_name']['even_id'] = "ID_no"

    fname = 'GLB_0360as_hazard_1950-2015.mat'
    path = INPUT_DIR / "windfields" / fname
    windfields = TropCyclone.from_mat(path, var_names=var_names)
    ibtracs_meta = [ibtracs_id2meta(i) for i in windfields.event_id]
    dates = [dt.date(*m[1:4]).toordinal() for m in ibtracs_meta]
    windfields.date = np.array(dates, dtype=np.int64)
    windfields.event_name = [m[0] for m in ibtracs_meta]
    windfields.event_id = np.arange(len(ibtracs_meta))

    # identify centroids in specified region
    lat, lon = windfields.centroids.lat, windfields.centroids.lon
    windfields.centroids.region_id \
        = shapely.vectorized.contains(region.shape, lon, lat)

    # select windfields in specified period and region
    if period is not None:
        period = (f"{period[0]}-01-01", f"{period[1]}-12-31")
    windfields = windfields.select(date=period, reg_id=1)

    return windfields


def emanuel_meta():
    """Generate/load DataFrame describing the available Emanuel windfield files"""
    meta_path = INPUT_DIR / "emanuel_fnames.csv"
    if meta_path.exists():
        return pd.read_csv(meta_path)

    pattern = "(temp_|Trial(?P<trial>[0-9])_GB_dk)" \
              "(?P<gcm>[0-9a-z]+)_?" \
              "((?P<rcp>piControl|20th|rcp[0-9]{2})cal)(|_full)_" \
              "(?P<hemisphere>N|S)_0360as\\.mat"
    prog = re.compile(pattern)
    files_l = []
    for path in INPUT_DIR.glob("windfields/*.mat"):
        fname = path.name
        match = prog.match(fname)
        if match is None:
            continue
        try:
            haz = h5py.File(path, "r")['hazard']
        except OSError:
            continue
        is_temp = (match.group("trial") is None)
        files_l.append({
            "basename": fname[:-13],
            "windfield_fname": fname,
            "minyear": int(haz['yyyy'][0, 0]),
            "maxyear": int(haz['yyyy'][-1, 0]),
            "gcm": gcm_trans_inv(match.group("gcm"), is_temp),
            "rcp": match.group("rcp"),
            "hemisphere": match.group("hemisphere"),
            "trial": 0 if is_temp else int(match.group("trial")),
            "tracks_per_year": 600 if is_temp else 300,
        })
    cols = ["basename", "windfield_fname", "minyear", "maxyear",
            "gcm", "rcp", "hemisphere", "trial", "tracks_per_year"]
    files_df = pd.DataFrame(files_l, columns=cols)
    files_df = files_df.sort_values(by=["gcm", "rcp", "minyear", "hemisphere"])
    files_df.to_csv(meta_path, index=None)
    return files_df


def emanuel_windfields(region, gcm=None, rcp=None, period=None, trial=None):
    """ Load pre-calculated windfields for simulated storm tracks

    Parameters
    ----------
    region : TCRegion object
        The geographical region to consider. This is not optional since
        windfields are separated by hemisphere.
    gcm : list of str, optional
        Name of GCMs, such as "MPI-ESM-MR".
    rcp : list of str, optional
        Name of RCPs, such as "rcp26". The historical data ("20th") doesn't need
        to be selected explicitly.
    period : pair of ints (minyear, maxyear), optional
        First and last year to consider.
    trial : list of int, optional
        Trials to include in the selection. By default, 0, 2 and 3 are excluded.

    Returns
    -------
    windfields : climada.hazard.TropCyclone object
    """
    meta = emanuel_meta()
    meta = meta[meta['hemisphere'] == region.hemisphere]

    if trial is None:
        trial = [1, 4]
    meta = meta[meta['trial'].isin(trial)]

    if gcm is not None:
        meta = meta[meta['gcm'].isin(gcm)]

    if rcp is not None:
        meta = meta[(meta['rcp'] == '20th') | meta['rcp'].isin(rcp)]

    # intersection with specified period
    if period is not None:
        meta = meta[(period[0] <= meta['maxyear']) & (meta['minyear'] <= period[1])]

    if meta.shape[0] == 0:
        raise Exception("Given gcm/rcp/period matches no trials!")

    hazards = []
    for _, row in meta.iterrows():
        path = INPUT_DIR / "windfields" / row['windfield_fname']
        haz = TropCyclone.from_mat(path)
        haz.event_name = [f"{row['windfield_fname']}-{n}" for n in haz.event_name]
        # some datasets include centroids beyond 60° that are irrelevant for TC hazards
        cutidx = 901186 if region.hemisphere == 'N' else 325229
        haz.centroids.region_id = np.zeros_like(haz.centroids.lat)
        haz.centroids.region_id[:cutidx] = 1
        haz = haz.select(reg_id=1)
        hazards.append(haz)
    windfields = TropCyclone.concat(hazards)

    # identify centroids in specified region
    lat, lon = windfields.centroids.lat, windfields.centroids.lon
    windfields.centroids.region_id \
        = shapely.vectorized.contains(region.shape, lon, lat)

    # select windfields in specified period and region
    if period is not None:
        period = (f"{period[0]}-01-01", f"{period[1]}-12-31")
    windfields = windfields.select(date=period, reg_id=1)

    return windfields


def emanuel_frequency_normalization(gcm, rcp, period=None, trial=None):
    """ Frequency normalization factors for given GCM and RCP

    Parameters
    ----------
    gcm : str
        Name of GCM, such as "MPI-ESM-MR".
    rcp : str
        Name of RCP, such as "rcp26".
    period : pair of ints (minyear, maxyear), optional
        First and last year to consider.
    trial : list of int, optional
        Trials to include in the selection. By default, 0, 2 and 3 are excluded.

    Returns
    -------
    freq_norm : DataFrame { year, freq }
        Information about the relative surplus of simulated events, i.e.,
        if `freq_norm` specifies the value 0.2 in some year, then it is
        assumed that the number of events simulated for that year is 5 times as
        large as it is estimated to be.
    """
    meta = emanuel_meta()
    meta = meta[meta['hemisphere'] == 'N']

    if trial is None:
        trial = [1, 4]
    meta = meta[meta['trial'].isin(trial)]

    meta = meta[(meta['gcm'] == gcm)]
    meta = meta[(meta['rcp'] == '20th') | (meta['rcp'] == rcp)]
    freq = []
    for _, row in meta.iterrows():
        path = INPUT_DIR / "tracks" / f"{row['basename']}.mat"
        tracks = scipy.io.loadmat(path, variable_names=['yearstore', 'freqyear'])
        freqyear = tracks['freqyear'].ravel()
        if freqyear[-1] == 0.0:
            freqyear[-1] = freqyear[-2]
        assert np.all(freqyear > 0)
        yearmin = tracks['yearstore'].min()
        years = np.arange(yearmin, yearmin + freqyear.size, dtype=np.int64)
        assert abs(np.unique(tracks['yearstore']).size - years.size) <= 1
        freq.append(pd.DataFrame({
            'year': years,
            'freq': freqyear / row['tracks_per_year'],
        }))
    freq = pd.concat(freq, ignore_index=True)
    if period is not None:
        freq = freq[(period[0] <= freq['year']) & (freq['year'] <= period[1])]
    freq = freq.sort_values(by=["year"]).reset_index(drop=True)
    return freq


def picontrol_mean(gcm, index):
    """Load mean value of pre-industrial control run for given GCM and climate index (e.g. GMT)

    Parameters
    ----------
    gcm : str
        Name of GCM, such as "MIROC5".
    index : str
        Name of index, such as "gmt" (see `climate_index`).
    """
    # for reference: 1850-1900 observed mean (in absolute Kelvin)
    # hist_temp = 273.15 + 13.75
    index_path = INPUT_DIR / "climate_index"
    minyear, maxyear = PICONTROL_PERIOD[gcm]
    path = index_path / (
        f"{index}-index_monthly_{gcm}-piControl_{minyear}-{maxyear}"
        f"_base-{minyear}-{maxyear}.csv"
    )
    if not path.exists():
        raise FileNotFoundError(f"Missing climate index data: {path}")
    return pd.read_csv(path)[index].mean()


def climate_index(gcm, rcp, index, running_mean=21):
    """Load time series of a climate index (e.g. GMT) for a given GCM/RCP

    The time period is 1861-2100 (1861-2299 for some of the rcp26 datasets)

    The data is concatenated from historical and future datasets, applying a
    21-year running mean in the case of GMT-based indices.

    CAUTION: For the GMT running mean, the data is *extended* at the edges by
    repeating the edge values; thereby any trend present in the data will
    become attenuated at the edges!

    GMT data is relative to piControl mean over a 500 year reference period.

    Parameters
    ----------
    gcm : str
        Name of GCM, such as "MPI-ESM-MR".
    rcp : str
        Name of RCP, such as "rcp26".
    index : str
        Name of index, one of ["gmt", "gmtTR", "esoi", "nao", "nino34", "pdo"],

            GMT : Global mean (surface) temperature
            GMT TR : GMT in the tropics, between -30 and +30 degrees latitude
            ESOI : El Nino southern oscillation index
            NAO : North Atlantic Oscillation
            NINO34 : Nino 3.4 sea surface temperature index
            PDO : Pacific decadal oscillation
    running_mean : int
        For GMT data, the running mean period. Defaults to 21.

    Returns
    -------
    cidx : DataFrame { year, month, `index` }
        Monthly data of given climate index.
    """
    index_path = INPUT_DIR / "climate_index"
    base_min, base_max, avg_interval = ({
        'gmt': (1971, 2000, ''),
        'gmtTR': (1971, 2000, ''),
        'esoi': (1950, 1979, '_3m'),
        'nao': (1950, 1979, '_3m'),
        'nino34': (1950, 1979, '_3m'),
        'pdo': (1971, 2000, ''),
    })[index]

    allmin = 1861
    allmax = 2299 if rcp == 'rcp26' else 2100

    cidx = []
    periods = [('historical', allmin, "*"), (rcp, "*", allmax)]
    for pname, minyear, maxyear in periods:
        fname = f"{index}-index_monthly_{gcm}-{pname}_{minyear}-{maxyear}" \
                f"_base-{base_min}-{base_max}{avg_interval}.csv"
        paths = sorted(index_path.glob(fname))
        if len(paths) == 0:
            raise FileNotFoundError(f"Missing climate index data: {index_path / fname}")
        path = paths[0]
        if index == 'pdo':
            tmp = pd.read_csv(path, delim_whitespace=True, skiprows=1, header=None)
            cols = ['time', 'pdo']
            tmp.columns = cols
        else:
            tmp = pd.read_csv(path)
            if 'Unnamed: 0' in tmp.columns:
                del tmp['Unnamed: 0']
        cidx.append(tmp)
    cidx = pd.concat(cidx)
    if cidx["time"].duplicated().any():
        print(f"Warning, duplicate entries for {index} {gcm} {rcp}:")
        print(cidx[cidx["time"].duplicated()])
        print("Taking the mean of duplicate entries...")
        cidx = cidx.groupby(by="time").mean().reset_index()
    year_month_day = cidx['time'].str.split("-", expand=True)
    cidx['year'] = year_month_day[0].astype(int)
    cidx['month'] = year_month_day[1].astype(int)
    cidx = cidx.drop(labels=['time'], axis=1)
    if cidx['year'].max() == 2099:
        # HadGEM2-ES stops at 2099 for rcp60
        cidx2100 = cidx[cidx['year'] == 2099]
        cidx2100['year'] = 2100
        cidx = pd.concat([cidx, cidx2100]).reset_index(drop=True)

    if index in ['gmt', 'gmtTR']:
        # define GMT change wrt piControl mean and apply running mean
        cidx[index] -= picontrol_mean(gcm, index)
        halfwin = running_mean // 2
        padded_data = np.pad(cidx[index].to_numpy(), halfwin, mode='edge')
        cidx[index] = np.convolve(padded_data, np.ones(running_mean) / running_mean, mode='valid')

    return cidx


def main(nrealizations, gcm, rcp, subbasin, draw_period, seed):
    """Main function used for processing command line calls"""
    # initialize random state
    climada_petals.hazard.emulator.random.random_state = np.random.RandomState(seed)

    # choose land area within specified TC basin
    reg = geo.TCRegion(tc_basin=subbasin)
    # Our input datasets are restricted to land area already, but usually we would need:
    # reg = geo.TCRegion(tc_basin=subbasin, country="all")

    norm_period = const.TC_BASIN_NORM_PERIOD[reg.tc_basin[:2]]
    windfields_obs = ibtracs_windfields(reg, period=norm_period)
    windfields_rcp = emanuel_windfields(reg, gcm=[gcm], rcp=[rcp])
    windfields_pool = windfields_rcp

    tc_events_obs = stats.haz_max_events(windfields_obs, min_thresh=MIN_WIND_MS)
    tc_events_rcp = stats.haz_max_events(windfields_rcp, min_thresh=MIN_WIND_MS)
    tc_events_pool = stats.haz_max_events(windfields_pool, min_thresh=MIN_WIND_MS)

    freq = emanuel_frequency_normalization(gcm, rcp)

    emu = emulator.HazardEmulator(tc_events_rcp, tc_events_obs, reg, freq,
                                  pool=emulator.EventPool(tc_events_pool))

    cidx = [climate_index(gcm, rcp, idx_name) for idx_name in ["gmt", "esoi"]]
    emu.calibrate_statistics(cidx)
    emu.predict_statistics(cidx)

    # use actual residuals in years that have been used in calibration
    year_mask = np.isin(emu.stats_pred['year'], emu.stats['year'])
    for explained in ["intensity_mean", "eventcount"]:
        sm_results = emu.fit_info[explained][0]
        emu.stats_pred.loc[year_mask, f'{explained}_residuals'] = sm_results.resid.to_numpy()

    draws = emu.draw_realizations(nrealizations, draw_period)

    timestamp = dt.datetime.now().strftime('%Y%m%d%H%M%S')
    dname = f"{timestamp}_{gcm}_{rcp}_{subbasin}_{draw_period[0]}_{draw_period[1]}_{nrealizations}"
    dname = OUTPUT_DIR / dname
    dname.mkdir(parents=True, exist_ok=True)

    tc_events_obs.to_csv(dname / "events_obs.csv", index=None)
    tc_events_rcp.to_csv(dname / "events_rcp.csv", index=None)
    tc_events_pool.to_csv(dname / "events_pool.csv", index=None)
    with open(dname / "emulator.pickle", "wb") as filep:
        pickle.dump(emu, filep)
    draws.to_csv(dname / "draws.csv", index=None)


if __name__ == "__main__":
    import argparse

    gcmls = list(GCM_TRANS.keys()) + list(GCM_TRANS_TEMP.keys())
    rcpls = ['rcp26', 'rcp60', 'rcp85']
    subbasinls = list(const.TC_SUBBASINS.keys()) + sum(const.TC_SUBBASINS.values(), [])

    parser = argparse.ArgumentParser(description='Draw subbasin samples.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM used in the TC generation.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP used in the TC generation.')
    parser.add_argument('subbasin', choices=subbasinls, metavar="SUBBASIN",
                        help='The (sub-)basin to sample for.')
    parser.add_argument('nrealizations', type=int, metavar="N",
                        help='Number of realizations per (sub-)basin.')
    parser.add_argument('period', type=int, metavar="YEAR", nargs=2, default=None,
                        help='Time period (minyear, maxyear).')
    parser.add_argument('--seed', type=int, metavar="INT", default=123456789,
                        help='Random seed for making draws.')
    args = parser.parse_args()

    main(args.nrealizations, args.gcm, args.rcp, args.subbasin, args.period, args.seed)
