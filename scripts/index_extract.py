
import argparse
import pathlib
import subprocess

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

OUTPUT_DIR = pathlib.Path("data") / "input" / "climate_index"
TEMP_DIR = pathlib.Path("temp")
PLOTS_DIR = TEMP_DIR / "plots"

CMIP5_DIR = pathlib.Path("/p/projects/climate_data_central/CMIP/CMIP5")
CMIP6_DIR = pathlib.Path("/p/projects/climate_data_central/CMIP/CMIP6")
TOBIAS_DIR = pathlib.Path("/p/projects/expact/tobias/index_from_GCM")

TEMP_MERGE_FILE = TEMP_DIR / "out_merge.nc"
TEMP_MERGE_HIST_FILE = TEMP_DIR / "out_merge_hist.nc"

PICONTROL_PERIOD_BIAS = {
    "GFDL-ESM2M": (1661, 2099),
    "IPSL-CM5A-LR": (1661, 2299),
    "MIROC5": (1661, 2299),
    "HadGEM2-ES": (1900, 2399),
}

PICONTROL_PERIOD = {
    "GFDL-ESM2M": (1, 500),
    "IPSL-CM5A-LR": (1800, 2299),
    "MIROC5": (2000, 2499),
    "HadGEM2-ES": (1900, 2399),
    "GFDL-ESM4": (1, 500),
    "IPSL-CM6A-LR": (1850, 2349),
    "MIROC6": (3200, 3699),
    "HadGEM3-GC31-MM": (1850, 2349),
    "HadGEM3-GC31-LL": (1850, 2349),
    "MPI-ESM1-2-HR": (1850, 2349),
    "MRI-ESM2-0": (1850, 2349),
    "UKESM1-0-LL": (1960, 2459),
}

CONFIG = {
    "esoi": {
        "var": "psl",
        "supervar": "Amon",
        "regions": {
            "r1": (230.0, 280.0, -5.0, 5.0),
            "r2": (90.0, 140.0, -5.0, 5.0),
        },
        "obs_var": "slp",
        "obs_file": "slp.ncep-rea.mon.mean.nc",
    },
    "nao": {
        "var": "psl",
        "supervar": "Amon",
        "regions": {
            "r1": (349.5, 352.0, 37.5, 39.5),
            "r2": (336.0, 338.5, 64.5, 66.5),
        },
        "obs_var": "slp",
        "obs_file": "slp.ncep-rea.mon.mean.nc",
    },
    "nino34": {
        "var": "tos",
        "supervar": "Omon",
        "regions": {"r1": (190.0, 240.0, -5.0, 5.0)},
        "obs_var": "sst",
        "obs_file": "sst.noaa.mnmean.v4.nc",
    },
    "gmt": {
        "var": "tas",
        "supervar": "Amon",
        "regions": {"r1": (0.0, 360.0, -90.0, 90.0)},
        "obs_var": "air",
        "obs_file": "air.noaa.mon.anom.nc",
    },
    "gmtTR": {
        "var": "tas",
        "supervar": "Amon",
        "regions": {"r1": (0.0, 360.0, -30.0, 30.0)},
        "obs_var": "air",
        "obs_file": "air.noaa.mon.anom.nc",
    },
}


def runmean(data, halfwin):
    """Simple running mean.

    CAUTION: Data is *extended* at the edges by repeating the edge values.
    Any trend present in the data will become attenuated at the edges!
    """
    window = 2 * halfwin + 1
    if window > data.size:
        raise ValueError(f"window ({window}) too large for data ({data.size})!")
    weights = np.ones(window) / window
    rm = np.convolve(np.pad(data, halfwin, mode="edge"), weights, "valid")
    return rm


def plot_index(idx, rcp, gcm, period, ref_period, index, index_sm):
    minyear, maxyear = period
    ref_minyear, ref_maxyear = ref_period

    nth_tick = index['time'].size // 10

    fig = plt.figure(figsize=(20, 10))
    ax = fig.gca()
    ax.plot(index.set_index("time"), c='tab:blue', marker="o", label="raw")
    ax.plot(index_sm.set_index("time"), c='tab:red', marker="o", label="smoothed")
    ax.set_xticks(index['time'].values[::nth_tick])
    ax.set_xlabel("time")
    ax.set_ylabel(idx)
    ax.legend()
    ax.set_title(
        f"{idx}-index, {minyear}-{maxyear} "
        f"(base {ref_minyear}-{ref_maxyear}), {gcm}, {rcp}"
    )

    path = PLOTS_DIR / (
        f"{idx}-index_monthly_{gcm}-{rcp}_{minyear}-{maxyear}"
        f"_base-{ref_minyear}-{ref_maxyear}.png"
    )
    print(f"Writing to {path} ...")
    fig.savefig(path, bbox_inches="tight")


def extract_index(idx, rcp, gcm, period, ref_period, bias=False):
    minyear, maxyear = period
    ref_minyear, ref_maxyear = ref_period

    # future time series with historic base period
    diff_per = minyear > 2005 and ref_maxyear <= 2005

    temp_dic = CONFIG[idx]

    # define smoothing
    smooth = "21y" if idx.startswith("gmt") else "3m"

    out_dic = {}

    path = None
    path_hist = None
    if rcp == "observation":
        path = TOBIAS_DIR / temp_dic["obs_file"]
        temp_dic["var"] = temp_dic["obs_var"]
    elif bias:
        path = TOBIAS_DIR / "bias_monmean" / gcm
    else:
        path = (
            CMIP5_DIR / rcp / temp_dic["supervar"] / temp_dic["var"] / gcm / "r1i1p1"
        )
        if not path.exists():
            cmip6_base = CMIP6_DIR / ("CMIP" if rcp in ["historical", "piControl"] else "ScenarioMIP")
            cmip6_paths = sorted(cmip6_base.glob(
                f"*/{gcm}/{rcp}/r1i1p1*/{temp_dic['supervar']}/{temp_dic['var']}/*/*"
            ))
            if len(cmip6_paths) == 0:
                raise FileNotFoundError(f"GCM-RCP combination doesn't exist: {gcm} {rcp}")
            path = cmip6_paths[0]

            if diff_per:
                path_hist = sorted(CMIP6_DIR.glob(
                    f"CMIP/*/{gcm}/historical/r1i1p1*/{temp_dic['supervar']}/{temp_dic['var']}/*/*"
                ))[0]
        elif diff_per:
            path_hist = (
                CMIP5_DIR / "historical" / temp_dic["supervar"] / temp_dic["var"]
                / gcm / "r1i1p1"
            )

    print(f"Reading GCM data from {path} ...")
    if path_hist is not None:
        print(f"Reading GCM reference data from {path_hist} ...")

    ### concatenate different time slices
    # / select desired period
    # / select desired regions
    # / average over regions

    TEMP_MERGE_FILE.unlink(missing_ok=True)
    TEMP_MERGE_HIST_FILE.unlink(missing_ok=True)

    cdo_cmd = lambda paths, outfile: ["cdo", "mergetime"] + paths + [outfile]
    cdo_params = []

    if rcp == "observation":
        cdo_params.append(([path], TEMP_MERGE_FILE))
    elif bias:
        cdo_params.append((list(path.glob(f"{temp_dic['var']}*.nc")), TEMP_MERGE_FILE))
    else:
        cdo_params.append((list(path.glob(f"{temp_dic['var']}_*.nc")), TEMP_MERGE_FILE))
        if diff_per:
            cdo_params.append(
                (list(path_hist.glob(f"{temp_dic['var']}_*.nc")), TEMP_MERGE_HIST_FILE)
            )

    for args in cdo_params:
        proc = subprocess.Popen(cdo_cmd(*args))
        proc.wait()

    for reg, bounds in temp_dic["regions"].items():
        cdo_mean_files = {
            "mean": TEMP_DIR / f"mean_{idx}_{reg}.txt",
            "ref_mean": TEMP_DIR / f"ref_mean_{idx}_{reg}.txt",
        }

        cdo_cmd_init = ["cdo", "-s", "outputtab,date,value"]
        cdo_cmd_box = [f"-sellonlatbox,{bounds[0]},{bounds[1]},{bounds[2]},{bounds[3]}"]
        cdo_cmd_var = [f"-selvar,{temp_dic['var']}"]
        cdo_cmd_meanop = ["-fldmean"] + (["-yearmean"] if idx.startswith("gmt") else [])

        with open(cdo_mean_files["ref_mean"], "w") as fp:
            cmd = (
                cdo_cmd_init + ["-timmean"] + cdo_cmd_meanop
                + (["-monmean"] if bias and not diff_per else [])
                + cdo_cmd_box + [f"-selyear,{ref_minyear}/{ref_maxyear}"]
                + cdo_cmd_var + [TEMP_MERGE_HIST_FILE if diff_per else TEMP_MERGE_FILE]
            )
            proc = subprocess.Popen(cmd, stdout=fp)
            proc.wait()

        with open(cdo_mean_files["mean"], "w") as fp:
            cmd = (
                cdo_cmd_init + cdo_cmd_meanop
                + (["-monmean"] if bias else [])
                + cdo_cmd_box + [f"-selyear,{minyear}/{maxyear}"]
                + cdo_cmd_var + [TEMP_MERGE_FILE]
            )
            proc = subprocess.Popen(cmd, stdout=fp)
            proc.wait()

        dfs = {
            f"{prefix}mean": pd.read_csv(
                cdo_mean_files[f"{prefix}mean"],
                sep=r"\s+",
                skiprows=1,
                header=None,
                engine="python",
            ) for prefix in ["", "ref_"]
        }
        for key, df in dfs.items():
            out_dic[f"{key}_{idx}_{reg}"] = df.iloc[:, 1].values

        mean_diff = out_dic[f"mean_{idx}_{reg}"] - out_dic[f"ref_mean_{idx}_{reg}"]

        # standard deviation around the long-term mean
        out_dic[f"std_{idx}_{reg}"] = np.sqrt(np.square(mean_diff).mean())

        # standardized anamoly
        out_dic[f"st_{idx}_{reg}"] = mean_diff / out_dic[f"std_{idx}_{reg}"]

        if "date" not in out_dic:
            out_dic["date"] = dfs['mean'].iloc[:, 0].values

    if len(temp_dic["regions"]) > 1:
        st_diff = out_dic[f"st_{idx}_r1"] - out_dic[f"st_{idx}_r2"]

        # final index
        ind = st_diff / np.sqrt(np.square(st_diff).mean())

        # running mean with edge values kept
        ind_sm = runmean(data=ind, halfwin=1)
    elif len(temp_dic["regions"]) == 1:
        if temp_dic["obs_var"] == "air":
            # this is for absolute GMT values
            ind = out_dic[f"mean_{idx}_r1"]
        else:
            ind = out_dic[f"mean_{idx}_r1"] - out_dic[f"ref_mean_{idx}_r1"]

        # running mean with edge values kept
        ind_sm = runmean(data=ind, halfwin=np.int64(smooth[:2]) - 1)
        if idx != "gmt" and idx != "gmtTR":
            ind_sm /= out_dic[f"std_{idx}_r1"]

    # raw index
    index = pd.DataFrame({f"{idx}": ind, "time": out_dic["date"]})
    path = OUTPUT_DIR / (
        f"{idx}-index_monthly_{gcm}-{rcp}_{minyear}-{maxyear}"
        f"_base-{ref_minyear}-{ref_maxyear}.csv"
    )
    print(f"Writing to {path} ...")
    index.to_csv(path)

    # smoothed index
    index_sm = pd.DataFrame({f"{idx}": ind_sm, "time": out_dic["date"]})
    path = OUTPUT_DIR / (
        f"{idx}-index_monthly_{gcm}-{rcp}_{minyear}-{maxyear}"
        f"_base-{ref_minyear}-{ref_maxyear}_{smooth}.csv"
    )
    print(f"Writing to {path} ...")
    index_sm.to_csv(path)

    return index, index_sm


def main():
    idxls = [
        'esoi',  # Equatorial SOI
        'nao',  # North Atlantic Oscillation
        'nino34',  # Nino 3.4 sea surface temperature index
        'gmt',  # Global mean (surface) temperature
        'gmtTR',  # GMT for the tropics only (between -30 and +30 lat)
    ]

    # GCMs (according to the folder names in CMIP* paths)
    cmip5_gcms = [p.name for p in CMIP5_DIR.glob("rcp*/Amon/tas/*")]
    cmip6_gcms = [p.name for p in CMIP6_DIR.glob("ScenarioMIP/*/*")]

    # ISIMIP2b GCMs: "GFDL-ESM2M", "HadGEM2-ES", "IPSL-CM5A-LR", "MIROC5",
    # Corresp. CMIP6: "GFDL-ESM4", "HadGEM3-GC31-MM", "IPSL-CM6A-LR", "MIROC6"
    # ISIMIP3b GCMs: "GFDL-ESM4", "IPSL-CM6A-LR", "MPI-ESM1-2-HR", "MRI-ESM2-0", "UKESM1-0-LL",
    gcmls = cmip5_gcms + cmip6_gcms + ["observation"]

    cmip5_rcps = ['piControl', 'historical', 'rcp26', 'rcp45', 'rcp60', 'rcp85']
    cmip6_rcps = sorted(set(p.name for p in CMIP6_DIR.glob("ScenarioMIP/*/*/*")))
    rcpls = cmip5_rcps + cmip6_rcps + ['observation']

    parser = argparse.ArgumentParser(
        description='Extract climate-index time series from NetCDF data.')

    parser.add_argument('index', choices=idxls, metavar="INDEX",
                        help='The climate index to compute.')
    parser.add_argument('gcm', choices=gcmls, metavar="GCM",
                        help='The GCM to use.')
    parser.add_argument('rcp', choices=rcpls, metavar="RCP",
                        help='The RCP to use.')

    # Example periods: (1861, 2005), (1950, 2005), (2006, 2100), (2015, 2100)
    parser.add_argument('--period', type=int, metavar="YEAR", nargs=2, default=None,
                        help='Time period (minyear, maxyear).')

    parser.add_argument('--ref-period', type=int, metavar="YEAR", nargs=2, default=None,
                        help='Refrence time period (minyear, maxyear) for normalization.')

    parser.add_argument('--bias', action="store_true",
                        help='Use ISIMIP2b bias-corrected data instead of original CMIP5 data.')

    args = parser.parse_args()

    if args.rcp == "observation" and args.gcm != "observation":
        raise ValueError("When using 'observation' as RCP, GCM must also be 'observation'.")

    if args.rcp.startswith("rcp") and args.gcm in cmip6_gcms:
        raise ValueError(f"Can't combine CMIP6 GCM with CMIP5 scenario: {args.gcm} {args.rcp}.")

    if args.rcp.startswith("ssp") and args.gcm in cmip5_gcms:
        raise ValueError(f"Can't combine CMIP5 GCM with CMIP6 scenario: {args.gcm} {args.rcp}.")

    # create directories
    TEMP_DIR.mkdir(parents=True, exist_ok=True)
    PLOTS_DIR.mkdir(parents=True, exist_ok=True)

    period = args.period
    if period is None:
        period = (1861, 2005) if args.gcm in cmip5_gcms else (1861, 2014)
        if args.rcp.startswith("ssp"):
            period = (2015, 2100)
        elif args.rcp.startswith("rcp"):
            period = (2006, 2299 if rcp == "rcp26" else 2100)

    ref_period = args.ref_period
    if ref_period is None:
        ref_period = (1971, 2000)
        if args.index in ["nao", "esoi", "nino34"]:
            ref_period = (1950, 1979)

    if args.rcp == "piControl":
        period = ref_period = (PICONTROL_PERIOD_BIAS if args.bias else PICONTROL_PERIOD)[args.gcm]
        print("Enforcing piControl (reference) period...")

    print(
        args.gcm, args.rcp, args.index,
        f"Period: {period[0]}-{period[1]}",
        f"(reference: {ref_period[0]}-{ref_period[1]})"
    )

    if args.bias:
        print("Using bias-corrected data from ISIMIP2b")

    fun_args = (args.index, args.rcp, args.gcm, period, ref_period)
    index, index_sm = extract_index(*fun_args, bias=args.bias)
    plot_index(*fun_args, index, index_sm)


if __name__ == "__main__":
    main()
