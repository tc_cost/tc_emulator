
import pathlib
import pickle

SOURCE_DIR = pathlib.Path(__file__).absolute().parent
DATA_DIR = SOURCE_DIR.parent / 'data'
INPUT_DIR = DATA_DIR / "input"
OUTPUT_DIR = DATA_DIR / "output"

"""Assign CMIP6 GCMs to each CMIP5 GCM"""
CMIP6_GCMS = {
    "GFDL-ESM2M": ["GFDL-ESM4"],
    "HadGEM2-ES": ["MRI-ESM2-0", "HadGEM3-GC31-MM", "HadGEM3-GC31-LL"],  # HadGEM3 doesn't have ssp119, -MM doesn't have ssp245
    "IPSL-CM5A-LR": ["IPSL-CM6A-LR"],
    "MIROC5": ["MIROC6"],
}

"""Assign a CMIP6 RCP to each CMIP5 RCP"""
CMIP6_RCPS = {
    "rcp26": "ssp119",
    "rcp60": "ssp245",
    "rcp85": "ssp585",
}

for path in OUTPUT_DIR.glob("*/emulator.pickle"):
    gcm, rcp = path.parent.name.split("_")[1:3]
    if gcm not in CMIP6_GCMS or rcp not in CMIP6_RCPS:
        continue
    scenario_gcms = CMIP6_GCMS[gcm]
    scenario_rcp = CMIP6_RCPS[rcp]
    for scenario_gcm in scenario_gcms:
        if scenario_rcp == "ssp119" and scenario_gcm.startswith("HadGEM3"):
            continue
        if scenario_rcp == "ssp245" and scenario_gcm == "HadGEM3-GC31-MM":
            continue
        name = f"{scenario_gcm}_{scenario_rcp}"
        scenario = INPUT_DIR / "scenarios" / f"{name}_GMT-path_Scenario_1950-2100.csv"
        print(
            f"{path.relative_to(SOURCE_DIR.parent)}"
            f" {scenario.relative_to(SOURCE_DIR.parent)}"
            f" 100 1980 2100 --name {name}"
        )
