"""
Visualize the climate indices that are used by the emulator to find statistical relationships
between climate and TC characteristics.
"""
import pathlib
import sys

import colorsys
import matplotlib.colors as mc
import matplotlib.pyplot as plt
import numpy as np

from climada_petals.hazard.emulator import const
from climada_petals.hazard.emulator import emulator
from climada_petals.hazard.emulator import geo
from climada_petals.hazard.emulator import stats

sys.path.append(pathlib.Path(__file__).absolute().parent)
from run_emulator import climate_index, GCM_TRANS


CMIP6_GCMS = {
    "GFDL-ESM2M": "GFDL-ESM4",
    "HadGEM2-ES": "MRI-ESM2-0",  # HadGEM3-GC31-LL doesn't have ssp119
    "IPSL-CM5A-LR": "IPSL-CM6A-LR",
    "MIROC5": "MIROC6",
}


def plot_gmt_timeseries():
    avg_season = const.TC_BASIN_SEASONS["NA"]
    fig, axs = plt.subplots(nrows=2, ncols=2, squeeze=False)
    fig.suptitle(f"GMT climate index for all GCMs")
    for gcm, ax in zip(CMIP6_GCMS.keys(), axs.ravel()):
        for rcp in ['rcp26', 'rcp60', 'rcp85', 'ssp119', 'ssp245', 'ssp585']:
            rcp_gcm = CMIP6_GCMS[gcm] if rcp.startswith("ssp") else gcm
            try:
                cidx = climate_index(rcp_gcm, rcp, "gmt")
            except FileNotFoundError:
                print(f"Skipping missing combination: {rcp_gcm} {rcp}")
                continue
            cidx = stats.seasonal_average(cidx, avg_season)
            cidx = cidx[(1980 <= cidx['year']) & (cidx['year'] <= 2100)]
            ax.plot(
                cidx['year'].values,
                cidx["gmt"].values,
                color="tab:blue" if rcp.startswith("rcp") else "tab:red",
                label=rcp,
            )
        ax.legend()
        ax.set_title(f"{gcm}/{rcp_gcm}")
    plt.show()


def lighten_color(color, amount=0.5):
    """ Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple."""
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])


def plot_esoi_timeseries():
    gcmls = list(GCM_TRANS.keys())
    rcpls = ['rcp26', 'rcp60', 'rcp85']
    basinls = ['EP', 'WP', 'NI', 'NA', 'SI', 'SP']
    basinls = ['SI']
    index = 'esoi'
    minyear, maxyear = (1980, 2100)
    running_mean = 51

    fig, axs = plt.subplots(nrows=2, ncols=2, sharey=True)
    colors = ['green', 'orange', 'red']

    for ax, gcm in zip(axs.ravel(), gcmls):
        for ibasin, basin in enumerate(basinls):
            season = const.TC_BASIN_SEASONS[basin]
            for cl, rcp in zip(colors, ['rcp26', 'rcp60', 'rcp85']):
                cl = lighten_color(cl, 0.5 + 0.12 * ibasin)
                cidx = climate_index(gcm, rcp, index)
                cidx = stats.seasonal_average(cidx, season)
                padded_data = np.pad(cidx[index].to_numpy(), running_mean // 2, mode='edge')
                cidx[index] = np.convolve(
                    padded_data, np.ones(running_mean) / running_mean, mode='valid')
                cidx = cidx[(minyear < cidx['year']) & (cidx['year'] < maxyear)]
                ax.plot(cidx['year'].values, cidx[index].values, c=cl, label=f"{rcp}-{basin}")
        ax.set_title(gcm)

    handles, labels = axs[0,0].get_legend_handles_labels()
    fig.legend(
        handles,
        labels,
        loc="upper center",
        bbox_to_anchor=(0.51, 1.02),
        ncol=len(rcpls) if len(basinls) == 1 else len(basinls),
        frameon=False,
        fontsize=10,
    )
    plt.show()


def plot_cidx_timeseries(gcm, subbasin, running_mean=None):
    avg_season = const.TC_BASIN_SEASONS[subbasin[:2]]
    cidxls = ['gmt', 'esoi']
    fig, axs = plt.subplots(nrows=1, ncols=len(cidxls), squeeze=False)
    fig.suptitle("GMT/ESOI climate indices for %s, %s" % (gcm, subbasin))
    for rcp in ['rcp26', 'rcp60', 'rcp85']:
        for index, ax in zip(cidxls, axs[0,:]):
            cidx = climate_index(gcm, rcp, index)
            cidx = stats.seasonal_average(cidx, avg_season)
            if index == 'esoi' and running_mean is not None:
                padded_data = np.pad(cidx[index].to_numpy(), running_mean // 2, mode='edge')
                cidx[index] = np.convolve(
                    padded_data, np.ones(running_mean) / running_mean, mode='valid')
            cidx = cidx[(1980 < cidx['year']) & (cidx['year'] < 2100)]
            ax.plot(cidx['year'].values, cidx[index].values, label=rcp)
            ax.legend()
            ax.set_title(index.upper())
    plt.show()


if __name__ == "__main__":
    # plot GMT timeseries for all GCMs (and RCPs/SSPs)
    plot_gmt_timeseries()

    # plot GMT and ESOI timeseries for one GCM and basin
    # plot_cidx_timeseries("MIROC5", "NIW", running_mean=51)

    # plot ESOI timeseries for all basins, RCPs and GCMs
    # plot_esoi_timeseries()



