#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=tce_precompute_wind
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_emulator/log
#SBATCH --output=tce_precompute_wind-%j.out
#SBATCH --error=tce_precompute_wind-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

# add --array=1-$(cat $1 | wc -l) to the sbatch call

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniconda3
export OMP_NUM_THREADS=16

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate climada_env

cd /home/tovogt/code/tc_emulator
echo $@> log/tce_precompute_wind-${SLURM_JOB_ID}.args
python scripts/precompute_windfields.py $@
