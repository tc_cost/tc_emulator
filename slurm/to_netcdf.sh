#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=tce_netcdf
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_emulator/log
#SBATCH --output=tce_netcdf-%j.out
#SBATCH --error=tce_netcdf-%j.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniconda3
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate climada_env

cd /home/tovogt/code/tc_emulator
echo $@ > log/tce_netcdf-${SLURM_JOB_ID}.args
python scripts/draws_to_nc.py $@
