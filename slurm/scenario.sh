#!/bin/bash

#SBATCH --qos=short
#SBATCH --job-name=tce_scenario
#SBATCH --account=ebm
#SBATCH --chdir=/home/tovogt/code/tc_emulator/log
#SBATCH --output=tce_scenario-%A_%a.out
#SBATCH --error=tce_scenario-%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1

# add --array=1-$(cat $1 | wc -l) to the sbatch call

module load intel/2019.0-nopython

export CONDA_HOME=/home/tovogt/.local/share/miniconda3
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

source $CONDA_HOME/etc/profile.d/conda.sh
conda activate climada_env

cd /home/tovogt/code/tc_emulator
ARGS=$(sed -n "${SLURM_ARRAY_TASK_ID}p" $1)
echo $ARGS > log/tce_scenario-${SLURM_ARRAY_JOB_ID}_${SLURM_ARRAY_TASK_ID}.args
python scripts/run_emulator_scenario.py $ARGS
