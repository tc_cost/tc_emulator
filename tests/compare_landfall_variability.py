
import sys
import numpy as np
import pandas as pd

fname1, fname2 = sys.argv[1:3]
df1, df2 = pd.read_csv(fname1), pd.read_csv(fname2)

df1['basin'].fillna("NA", inplace=True)
df2['basin'].fillna("NA", inplace=True)

print("Comparing output files:")
print(f"A = {fname1}")
print(f"B = {fname2}")

cols = []
for col in df2.columns:
    if col not in df1.columns:
        print(f"Column {col} is in B but not in A")
    else:
        cols.append(col)
df2 = df2[cols]

cols = []
for col in df1.columns:
    if col not in df2.columns:
        print(f"Column {col} is in A but not in B")
    else:
        cols.append(col)
df1 = df1[cols]
df2 = df2[cols]

df1 = df1.set_index(["TC_ID", "cty"])
df2 = df2.set_index(["TC_ID", "cty"])

l1, l2 = df1.shape[0], df2.shape[0]
if l1 != l2:
    print(f"Differing lengths A:{l1} and B:{l2}.")
    print(f"Only in A, but not in B:")
    print(df1.loc[df1.index.difference(df2.index)])
    print(f"Only in B, but not in A:")
    print(df2.loc[df2.index.difference(df1.index)])

idx = df1.index.intersection(df2.index)
df1 = df1.loc[idx]
df2 = df2.loc[idx]

for col, col_dt in zip(df1.columns, df1.dtypes):
    if str(col_dt) == 'object':
        msk = (df1[col] == df2[col])
    else:
        msk = (df1[col] - df2[col])**2 < 1e-5
    if not np.all(msk):
        print(f"Mismatch in column {col}:")
        msk = ~msk
        print(df1[msk][:3])
        print(df2[msk][:3])
        print("")

print("Check finished.")
