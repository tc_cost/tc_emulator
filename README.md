
# TC Emulator Scripts

Emulate tropical cyclones at different levels of global warming.

This set of scripts is built around the CLIMADA submodule `climada_petals.hazard.emulator`.
For more information about that core functionality, please refer to
the [CLIMADA docs](https://climada-petals.readthedocs.io/en/stable/tutorial/climada_hazard_emulator.html).
This set of scripts is basically a more complex variant of the example implemented in the CLIMADA documentation.

## Installation

This emulator is tested on Ubuntu 20.04 and on the PIK cluster (with SLURM), but should run on any
platform (e.g, Windows, macOS) that is supported by the CLIMADA Python package.

Install the Python project CLIMADA (including climada_petals) in a conda environment as specified
in the [CLIMADA documentation](https://climada-python.readthedocs.io/en/stable/guide/Guide_Installation.html).
All scripts contained in this repository can be executed within that environment.

## Execution

The heart of this project is the script `scripts/run_emulator.py` which learns tropical cyclone statistics from the
synthetic track sets by Kerry Emanuel (as available on request from the ISIMIP project).
After learning the statistics, it is able to draw new subsamples of the ISIMIP track sets that reproduce (emulate) the
deferred functional relationships between climate and TC characteristics.
By default, the generated subsamples will only be according to the exact climate conditions that were present in the
GCMs that have originally been used by Kerry Emanuel to generate the synthetic track sets.
This functionality is already very useful since the subsamples are better suited for Monte-Carlo approaches than the
original track sets. Furthermore, regional biases of the original track set are removed (by comparing with observational
statistics).

The script `scripts/run_emulator.py` needs 6 input parameters on the command line:
`GCM RCP SUBBASIN N YEAR_START YEAR_END`. For example:

```shell
python scripts/run_emulator.py HadGEM2-ES rcp60 NA 100 1980 2100
```
Use `scripts/run_emulator.py --help` in order to obtain a description of all the parameters.

The script `run_emulator.py` doesn't use the bare TC tracks as input data, but uses precomputed windfields. Currently,
it only supports a fixed set of windfields that originally have been precomputed using a very old MATLAB version of
CLIMADA.

There is already a draft of how a more recent version of CLIMADA could be used to generate a more up-to-date
version of those windfields in the script `scripts/precompute_windfields.py`. However, the script `run_emulator.py` is
currently not able to use those updated windfields.

Internally, each emulator object that is calibrated in `scripts/run_emulator.py` is pickled for use in other
applications. Currently, there is only one other use case for that: the script `run_emulator_baseline.py` uses the
pre-calibrated emulator to generate draws that reproduce a hypothetical baseline scenario, i.e. today's climate
conditions held fixed for a specified period of time.

The main emulator output are CSV files describing which TC events belong to each draw. These descriptive files can be
translated to NetCDF files that actually contain the gridded wind fields of each draw on a regional grid using the
script `scripts/draws_to_nc.py`. The script obtains the path of the CSV file as a command line argument and generates
a NetCDF file at the same location (the `.csv` file ending is replaced by `.nc`).

## Input/Output Data

Necessary input data (GMT timelines, generated and observed TC windfields, tracks etc.) will be loaded from
`./data/input/` while emulator results (primarily samples according to climate scenarios) are stored in subdirectories
of `./data/output/`:
```
data/
├── input
│   ├── climate_index
│   ├── scenarios
│   ├── tracks
│   │   └── normalization
│   └── windfields
└── output
```

## Randomness

Random draws are made using a pseudo-random number generator from numpy. The seed is fixed (hardcoded) in
`climada_petals.hazard.emulator.random` for scientific reproducibility. Changes to the seed shouldn't change the
qualitative results of this project.

## Parallelization

The emulator is calibrated for a specified region (most naturally, ocean basins). Runs for different regions are,
therefore, independent and can be easily parallelized.

Furthermore, if the emulator is to be applied to a global pool of TC tracks, an efficient workflow is to precompute
windfields for all tracks and store them in a permanent location. Once the windfield data is available, emulator runs
for arbitrary regions and time periods are comparably fast.

In the folder `slurm`, you find examples of how to run these scripts on a SLURM cluster.
